# Amazing Calendar

Amazing Calendar is a simple calendar project when you can add multiple tasks (reminders), with a description, date/time and the location of the reminder.

Also, you can select a specific color to a reminder that's displayed at calendar. The local weather forecast is also shown in the list.

# Installation

Amazing Calendar requires [NPM](https://www.npmjs.com/) to install dependencies and run project. You need a [Open Weather API](https://openweathermap.org/api) key to fetch weather correctly.

First of all, clone the repository.

Create a **.env** file in root directory. The file must contain the following content: (Where **YOUR_API** is your Open Weather API key)
```
REACT_APP_API_KEY={YOUR_API}
```

Install all of dependencies and run project.

```sh
$ cd amazing-calendar
$ npm install
$ npm start
```

License
----

MIT
