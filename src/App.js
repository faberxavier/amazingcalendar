import React from "react";
import Calendar from './components/calendar'
import { values } from './store/scheduleReducer'
import EventList from "./components/eventList";
import { useSelector } from 'react-redux'
const App = () => (

  <>
    <Calendar />
    <EventList events={useSelector(values)}></EventList>
  </>
)

export default App;