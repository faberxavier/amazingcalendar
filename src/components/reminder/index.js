import React from 'react'
import "./reminder.css";

const Reminder = ({ reminders }) => {
    return (
        <ul>
            {
                (reminders) && reminders.map((event, key) => {
                    return (< li key={key} style={{ borderColor: event.color, backgroundColor: event.color }} > {event.time + " " + event.reminder}</li>)
                })
            }
        </ul >
    )
}

Reminder.defaultProps = {
    type: 'days'
}

export default Reminder