import React, { useState } from 'react';
import ColorPicker from "../color-picker";
import Button from "../button";

import { string, bool, shape, number } from "prop-types";
import { useDispatch } from 'react-redux';
import { createEvent, updateWeatherAsync, removeByDate, updateEvent} from "../../store/scheduleReducer";

import './modal.css';

const Modal = (props) => {
	const [form, setForm] = useState({
		reminder: props.data.reminder,
		location: props.data.location,
		date: props.data.date,
		time: props.data.time,
		color: props.data.color
	});
	const dispatch = useDispatch();

	const updateForm = (key) => (event) => {
		setForm({ ...form, [key]: event.target.value })
	}
	const updateColor = (key) => (color) => {
		setForm({ ...form, [key]: color.hex })
	}
	const generateKey= (date) =>{
		return date.replace(/-/g, "")
	}
	const remindMe = () => {
		props.onClose();
		dispatch(createEvent(
			{
				dataKey: generateKey(form.date),
				data:
				{
					reminder: form.reminder,
					date: form.date,
					time: form.time,
					location: form.location,
					color: form.color
				}
			}
		));
		dispatch(updateWeatherAsync(form.location, form.date.replace(/-/g, "")))
		resetState()
	}
	const updateRemindDifferentsDates = () =>{
		dispatch(removeByDate({ dataKey: generateKey(props.data.date), arrayKey: props.data.arrayKey }))
		dispatch(createEvent(
			{
				dataKey: generateKey(form.date),
				data:
				{
					reminder: form.reminder,
					date: form.date,
					time: form.time,
					location: form.location,
					color: form.color
				}
			}
		));
		dispatch(updateWeatherAsync(form.location, form.date.replace(/-/g, "")))
		resetState()
	}
	const updateRemindDiffLocations = () =>{

		dispatch(updateEvent({

			dataKey: generateKey(form.date),
			arrayKey: props.data.arrayKey,
			data:
			{
				reminder: form.reminder,
				date: form.date,
				time: form.time,
				location: form.location,
				color: form.color
			}


		}))
		dispatch(updateWeatherAsync(form.location, form.date.replace(/-/g, "")))
		resetState()

	}
	const updateRemind = () => {
		if(props.data.date !== form.date){
			updateRemindDifferentsDates()
		}
		else if (props.data.location !== form.location){
			updateRemindDiffLocations()
		}
		else 
			dispatch(updateEvent({
				dataKey: generateKey(form.date),
				arrayKey: props.data.arrayKey,
				data:
				{
					reminder: form.reminder,
					date: form.date,
					time: form.time,
					location: form.location,
					color: form.color
				}
			}))
	}
	const resetState = () => {
		setForm(form, {
			reminder: '',
			location: '',
			date: '',
			time: '',
			color: '#FEFEFE'
		})
	}

	return (
		<div className="modal">
			<div className="modal-content" style={{ backgroundColor: form.color }}>
				<div className="close">
					<span className="close-icon" onClick={() => props.onClose()}>&times;</span>
				</div>

				<div className="form-align">
					<label className="form-label">Reminder
						<input className="form-input" onChange={updateForm('reminder')} value={form.reminder} name="reminder" type="text" placeholder="Reminder" maxLength="30" test-id="reminder" />
					</label>
					<label className="form-label">Location
						<input className="form-input" onChange={updateForm('location')} value={form.location} name="location" type="text" placeholder="Select a location" />
					</label>
					<label className="form-label">Date
						<input className="form-input" onChange={updateForm('date')} value={form.date} name="date" type="date" placeholder="Date" />
					</label>
					<label className="form-label">Time
						<input className="form-input" onChange={updateForm('time')} value={form.time} name="time" type="time" placeholder="Time" />
					</label>
					<div className="center">
						<ColorPicker onChange={updateColor('color')} />
					</div>
					{props.editMode ?
						<div className="center">
							<Button content="Edit Reminder" onClick={updateRemind} />
						</div>
						:
						<div className="center">
							<Button content="Remind-me" onClick={remindMe} />
						</div>
					}
				</div>
			</div>
		</div >
	)
}

Modal.propTypes = {
	data: shape({
		reminder: string,
		location: string,
		date: string,
		time: string,
		color: string,
		editMode: bool,
		arrayKey: number
	})
};

Modal.defaultProps = {
	data: {
		reminder: '',
		location: '',
		date: '',
		time: '',
		color: '#FFFFFF',
		editMode: false,
		arrayKey: undefined
	}
}

export default Modal;