import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Modal from '.';

jest.unmock('react-redux');

const mockRedux = require('react-redux');
mockRedux.useDispatch = () => jest.fn().mockReturnValue("");

describe('Modal', () => {
    const defaultProps = {
        data: {
            reminder: '',
            location: '',
            date: '',
            time: '',
            color: '#FFFFFF',
            editMode: false
        }
    };

    const renderComponent = (props) => shallow(
        <Modal {...defaultProps} {...props} />
    );

    it('renders modal component without crashing', () => {
        const wrapper = renderComponent();
        expect(wrapper.exists()).toBeTruthy();
    });

    it('matches the snapshot', () => {
        const tree = renderComponent();
        expect(toJson(tree)).toMatchSnapshot();
    });

    it('should not let users type more than 30 characters on reminder name', () => {
        const wrapper = renderComponent();
        const inputFieldReminder = wrapper.find("[test-id='reminder']");
        inputFieldReminder.simulate('change', { target: { value: 'abcdefghijklmnopqrstuvxyz12345678900asgfaw' } });
        expect(inputFieldReminder.prop('maxLength')).toBe("30");
    })
}); 
