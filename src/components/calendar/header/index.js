import React from 'react'
import * as dateFns from "date-fns";
import { func, instanceOf } from 'prop-types';
import { ptBR } from 'date-fns/locale';


const HeaderCalendar = ({ currentDate, nextMonth, prevMonth }) => {

    const DATE_FORMAT = "MMMM yyyy";
    return (

        <div className="header row flex-middle">
            <div className="column col-start">
                <div className="icon" onClick={prevMonth}>
                    chevron_left
                </div>
            </div>
            <div className="column col-center center">
                <span>{dateFns.format(currentDate, DATE_FORMAT, { locale: ptBR })}</span>
            </div>
            <div className="column col-end right">
                <div className="icon" onClick={nextMonth}>
                    chevron_right
					 </div>
            </div>
        </div>)
}

HeaderCalendar.propTypes = {
    currentDate: instanceOf(Date),
    nextMonth: func.isRequired,
    prevMonth: func.isRequired
}

export default HeaderCalendar