import React from 'react'
import * as dateFns from "date-fns";
import { ptBR } from 'date-fns/locale';
import { instanceOf } from 'prop-types';

const Week = ({ currentDate }) => {

    const dateFormat = "E";
    const days = [];
    let startDate = dateFns.startOfWeek(currentDate);
    for (let i = 0; i < 7; i++) {
        days.push(
            <div className="column col-center" key={i}>
                {dateFns.format(dateFns.addDays(startDate, i), dateFormat, { locale: ptBR })}
            </div>
        );
    }
    return <div className="days row">{days}</div>;
};


Week.propTypes = {
    currentDate: instanceOf(Date),
}


export default Week