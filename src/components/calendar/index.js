import React, { useState } from 'react'
import "./calendar.css";
import HeaderCalendar from "./header";
import Days from "./days";
import Week from "./week"
import Modal from "../modal";
import * as dateFns from "date-fns";

const Calendar = () => {
	const [currentDate, setCurrentDate] = useState(new Date());
	const [display, setDisplay] = useState(false);
	const [dateFromCalendar, setDate] = useState();

	const showModal = dateSelected => {
		setDisplay(!display);
		setDate(dateFns.format(dateSelected, 'yyyy-MM-dd'));
	}

	const onClose = () => {
		setDisplay(false);
	};

	const nextMonth = () => {
		setCurrentDate(dateFns.addMonths(currentDate, 1));
	};

	const prevMonth = () => {
		setCurrentDate(dateFns.subMonths(currentDate, 1));
	};
	return (
		<div>
			<div className="calendar">
				<HeaderCalendar currentDate={currentDate} nextMonth={() => nextMonth()} prevMonth={() => prevMonth()}></HeaderCalendar>
				<Week currentDate={currentDate}></Week>
				<Days currentDate={currentDate} showModalWithDateSelected={(dateSelected) => { showModal(dateSelected) }}></Days>
			</div>
			{display && <Modal onClose={() => onClose()} data={{ date: dateFromCalendar}} />}
		</div>
	)

}

export default Calendar;