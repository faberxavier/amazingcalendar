import React, { useState } from 'react'
import * as dateFns from "date-fns";
import { instanceOf } from 'prop-types';
import { useSelector } from 'react-redux';
import { values } from '../../../store/scheduleReducer'

import "../calendar.css";
import Reminder from '../../reminder';


const Days = (props) => {
    const [selectedDate] = useState(new Date());
    const monthStart = dateFns.startOfMonth(props.currentDate);
    const monthEnd = dateFns.endOfMonth(monthStart);
    const startDate = dateFns.startOfWeek(monthStart);
    const endDate = dateFns.endOfWeek(monthEnd);
    const dateFormat = "d";
    const rows = [];
    let days = [];
    let day = startDate;
    let formattedDate = "";

    const reminders = useSelector(values)

    const dateToKey = date => {
        return dateFns.format(date, 'yyyyMMdd')
    }

    while (day <= endDate) {
        for (let i = 0; i < 7; i++) {
            formattedDate = dateFns.format(day, dateFormat);
            const cloneDay = day;
            days.push(
                <div
                    className={`column list cell ${!dateFns.isSameMonth(day, monthStart)
                        ? "disabled" : dateFns.isSameDay(day, selectedDate)
                            ? "selected" : ""}
												${ i === 0 || i === 6 ? "weekend" : ""}`}
                    onClick={() => props.showModalWithDateSelected(cloneDay)} key={day}
                >
                    {(reminders[dateToKey(day)]) && <Reminder reminders={reminders[dateToKey(day)]} />}
                    <span className="number">{formattedDate}</span>
                    <span className="bg">{formattedDate}</span>
                </div>
            );
            day = dateFns.addDays(day, 1);
        }
        rows.push(
            <div className="row" key={day}> {days} </div>
        );
        days = [];
    }
    return <div className="body">{rows}</div>;
}

Days.propTypes = {
    currentDate: instanceOf(Date),
}


export default Days