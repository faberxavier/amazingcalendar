import React, { useState } from "react";
import { CirclePicker } from "react-color";
import Button from "../button";
import "./color-picker.css";

const ColorPicker = (props) => {

	const [displayColorPicker, setDisplayColorPicker] = useState(false);

	const handleClick = () => {
		setDisplayColorPicker(true);
		if (displayColorPicker ? handleClose() : null);
	};

	const handleClose = () => {
		setDisplayColorPicker(false);
	};

	return (
		<div>
			<Button content="Pick a Color" onClick={handleClick} />
			{displayColorPicker ? <div className="popover">
				<div className="cover" onClick={handleClose} />
				<CirclePicker onChange={props.onChange} />
			</div> : null}
		</div>
	)

}

export default ColorPicker;