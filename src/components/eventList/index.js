import React, { useState }  from 'react'
import { string, shape } from 'prop-types';
import Modal from "../modal";

import "./eventList.css"
const EventList = ({ events }) => {
    const [display, setDisplay] = useState(false);
    const [modalData, setModalData] = useState(undefined);
    const showModal = e => {
        setDisplay(!display);
    }

    const onClose = () => {
        setDisplay(false);
    };

    const openModalWithReminderInfo = (reminder,key) => {
        showModal()
        setModalData({...reminder,'arrayKey':key})
    };

    return (
        <>
            <table className="event-table">
                <thead>
                    <tr>
                        <th>Color</th>
                        <th>Name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Local</th>
                        <th>Weather</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        (events) && Object.values(events).map((listItem, index) => (
                            listItem.map((reminder, key) => {
                                return (
                                    <tr key={key} id={`${index}-${key}`} onClick={() => openModalWithReminderInfo(reminder,key)}>
                                        <td><div className="event-color" style={{ backgroundColor: reminder.color }}></div></td>
                                        <td><div >{reminder.reminder}</div></td>
                                        <td><div >{reminder.date}</div></td>
                                        <td><div >{reminder.time}</div></td>
                                        <td><div >{reminder.location}</div></td>
                                        <td><div >{reminder.weather}</div></td>
                                    </tr>)
                            })
                        ))
                    }
                </tbody>
            </table >
            {display && <Modal onClose={() => onClose()} data={modalData} editMode />}

        </>
    )
}

EventList.propTypes = {
    events:
        shape({
            reminder: string,
            date: string,
            time: string,
            local: string,
            description: string
        }).isRequired
}


export default EventList