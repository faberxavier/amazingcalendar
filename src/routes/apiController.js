const OPEN_WEATHER_MAP_API = process.env.REACT_APP_API_KEY;

export const getWeatherFromAPI = async (cityName) => {
    return await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${cityName}&appid=${OPEN_WEATHER_MAP_API}`)
        .then(res => res.json())
        .then((data) => (
            {
                weather: data.weather[0].description
            }
        ))
        .catch((e) => (
            { weather: 'Unknown' })
        )
}
