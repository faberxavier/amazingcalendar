import { createSlice } from '@reduxjs/toolkit'
import { getWeatherFromAPI } from '../routes/apiController'

export const schedule = createSlice({
    name: 'schedule',
    initialState: {
        reminders: {},
    },
    reducers: {
        createEvent: (state, action) => {
            if (state.reminders[action.payload.dataKey])
                state.reminders[action.payload.dataKey].push(action.payload.data)
            else {
                state.reminders[action.payload.dataKey] = [action.payload.data]
            }
        },
        updateEvent: (state, action) => {
            const newArray = [...state.reminders[action.payload.dataKey]]
            newArray[action.payload.arrayKey] = action.payload.data
            return {
                ...state,
                    reminders: {
                        [action.payload.dataKey]: newArray,
                }
            }
         },
         removeByDate: (state,action) => {
             return {  // returning a copy of orignal state
                 ...state, //copying the original state
                 reminders: {
                     [action.payload.dataKey]: state.reminders[action.payload.dataKey]
                         .filter(reminde => reminde.id !== action.payload.arrayKey)
                     }
                 // returns a new filtered todos array
             }
         },
        updateWeather: (state, action) => {
            state.reminders[action.payload.keyReminder].map((reminderOption) => {
                if (reminderOption.location === action.payload.targetCity) {
                    reminderOption.weather = action.payload.weather
                }
                else {
                    reminderOption.weather = "Unknown"
                }
            })
        },
    }

})

export const { createEvent, removeByDate, updateEvent, updateWeather } = schedule.actions

export const updateWeatherAsync = (city, key) => async dispatch => {
    let weatherCity = (typeof city === 'string' ? city.replace(/ /g, "+") : '');
    getWeatherFromAPI(weatherCity).then((resultApi) => {
        dispatch(updateWeather({
            keyReminder: key,
            targetCity: weatherCity,
            weather: resultApi.weather
        }))

    })
}
export const values = state => state.schedule.reminders;

export default schedule.reducer