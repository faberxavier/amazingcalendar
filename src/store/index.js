import { configureStore } from '@reduxjs/toolkit'

import scheduleReducer from './scheduleReducer'

export default configureStore({
    reducer: {
        schedule: scheduleReducer
    }
})