import configureStore from 'redux-mock-store';
import scheduleReducer, { createEvent, updateWeather } from './scheduleReducer';

const mockStore = configureStore();
const store = mockStore();

describe('Schedule Reducer', () => {
    beforeEach(() => {
        store.clearActions();
    });

    describe('actions', () => {
        it('should dispatch createEvent', () => {
            const payload = { name: "payloadEvent", value: "object" };

            const expectedAction = [{
                type: "schedule/createEvent",
                payload
            }];

            store.dispatch(createEvent(payload));
            expect(store.getActions()).toEqual(expectedAction);
        });

        it('should dispatch updateWeather', () => {
            const payload = { local: "London", targetCity: "london" };

            const expectedAction = [{
                type: "schedule/updateWeather",
                payload
            }];

            store.dispatch(updateWeather(payload));
            expect(store.getActions()).toEqual(expectedAction);
        });

    });

    describe('reducer', () => {
        it('should return a function', () => {
            expect(scheduleReducer).toBeInstanceOf(Function);
        });

        describe('default state', () => {
            it('should return default state', () => {
                const defaultState = scheduleReducer(undefined, { type: "UNKNOWN" });

                expect(defaultState).toEqual({ reminders: {} });
            });
        })

        describe('action createEvent', () => {
            it('should create new entry on reminders', () => {
                const initialState = {
                    reminders: {}
                };

                const expectedState = {
                    reminders: {
                        "20200212": [{
                            reminder: "reminder-test",
                            date: "12-02-2020",
                            time: "12:12",
                            location: "Brazil",
                            color: "#FF0000"
                        }],
                    }
                }

                const payload = {
                    dataKey: "20200212",
                    data: {
                        reminder: "reminder-test",
                        date: "12-02-2020",
                        time: "12:12",
                        location: "Brazil",
                        color: "#FF0000"
                    }
                };

                const action = {
                    type: "schedule/createEvent",
                    payload
                };

                const newState = scheduleReducer(initialState, action);
                expect(newState).toEqual(expectedState)
            });

            it('should update weather field when receive response from external api', () => {
                const initialState = {
                    reminders: {
                        "20200212": [{
                            reminder: "reminder-test",
                            date: "12-02-2020",
                            time: "12:12",
                            location: "Brazil",
                            color: "#FF0000"
                        }],
                    }
                };

                const expectedState = {
                    reminders: {
                        "20200212": [{
                            reminder: "reminder-test",
                            date: "12-02-2020",
                            time: "12:12",
                            location: "Brazil",
                            color: "#FF0000",
                            weather: "scattered clouds"
                        }],
                    }
                }

                const payload = {
                    keyReminder: "20200212",
                    targetCity: "Brazil",
                    weather: "scattered clouds"
                };

                const action = {
                    type: "schedule/updateWeather",
                    payload
                };

                const newState = scheduleReducer(initialState, action);
                expect(newState).toEqual(expectedState)
            });
        });
    });
});
